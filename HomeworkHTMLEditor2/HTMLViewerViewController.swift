//
//  HTMLViewerViewController.swift
//  HomeworkHTMLEditor2
//
//  Created by user153878 on 5/7/19.
//  Copyright © 2019 Leonid Osym. All rights reserved.
//
import Foundation
import UIKit
import WebKit

class HTMLViewerViewController: UIViewController, WKUIDelegate{
    
    var htmlCode: String?
    let webView = WKWebView()
    
    override func loadView() {
        self.view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.uiDelegate = self
        //webView.size
        if htmlCode != nil {
            // because size is too small on iPhone screen
            htmlCode?.append("<style> body { font-size: 400%; } </style>")
            webView.loadHTMLString(htmlCode!, baseURL: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
}
