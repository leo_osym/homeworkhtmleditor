//
//  HTMLDocument.swift
//  HomeworkHTMLEditor2
//
//  Created by user153878 on 5/7/19.
//  Copyright © 2019 Leonid Osym. All rights reserved.
//

import Foundation
import UIKit

class HTMLDocument: UIDocument {
    
    var text: String?
    
    override func contents(forType typeName: String) throws -> Any {
        if let content = text {
            
            let length = content.lengthOfBytes(using: String.Encoding.utf8)
            return NSData(bytes:content, length: length)
        } else {
            return Data()
        }
    }
    
    override func load(fromContents contents: Any, ofType typeName: String?) throws {
        if let userContent = contents as? Data, userContent.count > 0 {
            text = NSString(bytes: (contents as AnyObject).bytes,
                            length: userContent.count,
                            encoding: String.Encoding.utf8.rawValue) as? String
        }
    }
}
