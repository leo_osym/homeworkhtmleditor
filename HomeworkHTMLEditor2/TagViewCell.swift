//
//  TagViewCell.swift
//  HomeworkHTMLEditor2
//
//  Created by user153878 on 5/8/19.
//  Copyright © 2019 Leonid Osym. All rights reserved.
//

import Foundation
import UIKit

class TagViewCell: UICollectionViewCell
{
    @IBOutlet weak var title: UILabel!
    var content: String?
}
