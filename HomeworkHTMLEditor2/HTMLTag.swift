//
//  HTMLTag.swift
//  HomeworkHTMLEditor2
//
//  Created by user153878 on 5/8/19.
//  Copyright © 2019 Leonid Osym. All rights reserved.
//

import Foundation
import UIKit

public class HTMLTag
{
    var title: String
    var content: String
    
    init(withTitle title: String, withContent content: String){
        self.title = title
        self.content = content
    }
}
