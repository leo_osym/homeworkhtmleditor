//
//  EditorViewController.swift
//  HomeworkHTMLEditor2
//
//  Created by user153878 on 5/7/19.
//  Copyright © 2019 Leonid Osym. All rights reserved.
//

import UIKit

class EditorViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDragDelegate, UICollectionViewDropDelegate {
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        save()
        dismiss(animated: true) {
            self.document?.close()
        }
    }
    
    @IBAction func closeWithoutSaving(_ sender: UIBarButtonItem) {
        dismiss(animated: true) {
            self.document?.close()
        }
    }
    
    private func save() {
        document?.text = editorField.text
        if document?.text != nil {
            self.document?.updateChangeCount(.done)
        }
    }
    
    @IBAction func showInBrowser(_ sender: UIBarButtonItem) {
        save()
        performSegue(withIdentifier: "ShowMyHTML", sender: sender)
        document?.close()
    }
    
    @IBOutlet weak var editorField: UITextView!
    var document: HTMLDocument?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        document?.open { success in
            if success {
                self.editorField.text = self.document?.text
            }
        }
    }
    
    
    @IBOutlet weak var tagCollection: UICollectionView!{
        didSet{
            tagCollection.dataSource = self
            tagCollection.delegate = self
            tagCollection.dragDelegate = self
            tagCollection.dropDelegate = self
            tagCollection.dragInteractionEnabled = true
        }
    }
    

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowMyHTML" {
            if let htmlView = segue.destination as? HTMLViewerViewController {
                htmlView.htmlCode = document?.text
            }
        }
    }
    
    // MARK: - CollectionView settings
    
    public var tags: [HTMLTag] = [HTMLTag(withTitle: "h1", withContent: "<h1></h1>"),
                                        HTMLTag(withTitle: "a", withContent: "<a href=\"#\"></a>"),
                                        HTMLTag(withTitle: "p", withContent: "<p></p>"),
                                        HTMLTag(withTitle: "br", withContent: "</br>"),
                                        HTMLTag(withTitle: "b", withContent: "<b></b>")]
    
    private var font: UIFont{
        return UIFontMetrics(forTextStyle: .body).scaledFont(for: UIFont.preferredFont(forTextStyle: .body).withSize(20.0))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCell", for: indexPath)
        if let tagCell = cell as? TagViewCell {
            let title = NSAttributedString(string: tags[indexPath.item].title, attributes: [.font:font])
            tagCell.title.attributedText = title
            tagCell.content = tags[indexPath.item].content
        }
        return cell
    }
    
    // MARK: - CollectionView drag
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
    
    private func dragItems(at indexPath: IndexPath) -> [UIDragItem]{
        if let attributedString = (tagCollection.cellForItem(at: indexPath) as? TagViewCell)?.content {
            let dragItem = UIDragItem(itemProvider: NSItemProvider(object: attributedString as NSItemProviderWriting))
            dragItem.localObject = tags[indexPath.item]
            return [dragItem]
        } else {
            return []
        }
    }
    
    // MARK: - CollectionView drop
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSAttributedString.self)
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if session.items[0].localObject != nil {
            return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
        }
        return UICollectionViewDropProposal(operation: .cancel)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: 0, section: 0)
        if let sourceIndexPath = coordinator.items[0].sourceIndexPath{
            if let htmlTag = coordinator.items[0].dragItem.localObject as? HTMLTag {
                collectionView.performBatchUpdates({
                    tags.remove(at: sourceIndexPath.item)
                    tags.insert(HTMLTag(withTitle: htmlTag.title, withContent: htmlTag.content), at: destinationIndexPath.item)
                    collectionView.deleteItems(at: [sourceIndexPath])
                    collectionView.insertItems(at: [destinationIndexPath])
                })
                coordinator.drop(coordinator.items[0].dragItem, toItemAt: destinationIndexPath)
            }
        }
    }
}

// find out if my controller is in temp navigationController
extension UIViewController {
    var contents: UIViewController {
        if let navcon = self as? UINavigationController {
            return navcon.visibleViewController ?? navcon
        } else {
            return self
        }
    }
}
